import pandas as pd
import numpy as np
# Import flask dependencies
from flask import Blueprint, request, render_template, g, redirect, url_for, jsonify

# Define the blueprint population
population = Blueprint('population', __name__)

@population.route('/')
def index():
	return redirect(url_for('population.per_arr'))

@population.route('/per_arr/', methods=['GET', 'POST'])
def per_arr():
    return render_template('population/per_arr.html')

@population.route('/per_region/', methods=['GET', 'POST'])
def per_region():
	return render_template('population/per_region.html')

@population.route('/per_square_km/', methods=['GET', 'POST'])
def per_square_km():
	return render_template('population/per_square_km.html')

@population.route('/per_arr/getPopulationData', methods=['GET'])
def getPopulationData():
	residents_file = 'app/static/data_processed/population/population_residents.csv'
	employees_file = 'app/static/data_processed/population/population_employees.csv'
	residents = pd.read_csv(residents_file).sort('arr_id', ascending=1)
	employees = pd.read_csv(employees_file).sort('arr_id', ascending=1)
	result = {'residents': residents.values.tolist(), 'employees': employees.values.tolist()}

	return jsonify(result)

@population.route('/per_region/getRegionPopulationData', methods=['GET'])
def getRegionPopulationData():
	residents_file = 'app/static/data_processed/population/population_residents.csv'
	employees_file = 'app/static/data_processed/population/population_employees.csv'
	senegal_arr_file = 'app/static/senegal_map/SENEGAL_ARR_V2.csv'
	senegal_reg_file = 'app/static/senegal_map/SENEGAL_REGIONS.csv'

	senegal_arr = pd.read_csv(senegal_arr_file)
	senegal_reg = pd.read_csv(senegal_reg_file)
	senegal = pd.merge(senegal_arr, senegal_reg, how='left', on=['REG', 'REG'])

	residents = pd.read_csv(residents_file).sort('arr_id', ascending=1)
	employees = pd.read_csv(employees_file).sort('arr_id', ascending=1)

	residents = residents.merge(
			senegal,
			left_on='arr_id',
			right_on='ARR_ID',
			how='left')

	employees = employees.merge(
			senegal,
			left_on='arr_id',
			right_on='ARR_ID',
			how='left')

	residents = residents.groupby(['REG_ID'], as_index=False)\
		.agg({'number_of_residents': np.sum, 'number_of_residents_in_percents': np.sum})
	residents['number_of_residents_in_percents'] = residents['number_of_residents_in_percents'].apply(lambda x: round(x, 2))

	employees = employees.groupby(['REG_ID'], as_index=False)\
		.agg({'number_of_employees': np.sum, 'number_of_employees_in_percents': np.sum})
	employees['number_of_employees_in_percents'] = employees['number_of_employees_in_percents'].apply(lambda x: round(x, 2))

	result = {'residents': residents.values.tolist(), 'employees': employees.values.tolist()}

	return jsonify(result)

@population.route('/per_square_km/calcPopulationPerSquareKm', methods=['GET'])
def calcPopulationPerSquareKm():
	residents_file = 'app/static/data_processed/population/population_residents.csv'
	employees_file = 'app/static/data_processed/population/population_employees.csv'
	senegal_arr_file = 'app/static/senegal_map/SENEGAL_ARR_V2.csv'
	senegal_reg_file = 'app/static/senegal_map/SENEGAL_REGIONS.csv'

	senegal_arr = pd.read_csv(senegal_arr_file)
	senegal_reg = pd.read_csv(senegal_reg_file)
	senegal = pd.merge(senegal_arr, senegal_reg, how='left', on=['REG', 'REG'])

	residents = pd.read_csv(residents_file).sort('arr_id', ascending=1)
	employees = pd.read_csv(employees_file).sort('arr_id', ascending=1)

	residents = residents.merge(
			senegal,
			left_on='arr_id',
			right_on='ARR_ID',
			how='left')

	employees = employees.merge(
			senegal,
			left_on='arr_id',
			right_on='ARR_ID',
			how='left')

	# calculate residents density
	residents = residents.groupby(['REG_ID'], as_index=False)\
		.agg({'number_of_residents': np.sum, 'number_of_residents_in_percents': np.sum})

	residents = residents.merge(
			senegal_reg,
			left_on='REG_ID',
			right_on='REG_ID',
			how='left')

	density = residents.apply(calc_density, axis=1)
	residents['density'] = density
	residents = residents.sort('density', ascending=1)
	
	return jsonify(result=residents.values.tolist())

def calc_density(group):
	area = group['AREA']
	residents_num = group['number_of_residents']

	return residents_num / area