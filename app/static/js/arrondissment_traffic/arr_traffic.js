var arr_traffic = {
    map: null,
    slider_dates: [],
    autoplay_timeout: null,
    months_list: [],

    init: function() {
        months_list = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        selectMenuItem('menu_arr_traffic');
        $('#loading').removeClass('hidden');
        autoplay_timeout = undefined,
        arr_traffic.setupDateSlider();
        arr_traffic.events();
    },

    events: function() {
        $('#autoplay').change(function() {
            var $this = $(this);
            if ($this.is(':checked')) {
                $('#choose_date').slider('disable');
                autoplay_timeout = window.setInterval(function() {
                    arr_traffic.getTrafficData(true);
                }, 2500);
            } else {
                $('#choose_date').slider('enable');
                window.clearInterval(autoplay_timeout);
                autoplay_timeout = undefined;
            }
        });

        $('#traffic').on('change', function() {
            arr_traffic.optionsChange('traffic');
        });
        
        $('#direction').on('change', function() {
            arr_traffic.optionsChange('direction');
        });
        
        $('#month').on('change', function() {
            arr_traffic.optionsChange('month');
        });

        $('#date_filter').on('change', function() {
            arr_traffic.optionsChange('date_filter');
        });

        $(window).on('resize', function() {
            map.resize();
        });
    },

    setupDateSlider: function() {
        var data = {
            direction: $('#direction').val(),
            month: $('#month').val(),
            month_raw: $('#month').find(':selected').data('raw'),
            date_filter: 'hour'
        };

        $.when($.ajax({
            url: $SCRIPT_ROOT + 'getDateSliderValues',
            data: data,
            type: 'post',
            dataType: 'json'
        }))
        .then(function(response) {
            slider_dates = response.result;
            $('#choose_date').slider({
                value: 0,
                min: 0,
                max: slider_dates.length - 1,
                step: 1,
                create: function(event, ui) {
                    $('#selected_date').val(slider_dates[0]);
                },
                slide: function(event, ui) {
                    $('#selected_date').val(slider_dates[ui.value]);
                },
                change: function(event, ui) {
                    var date_filter = $('#date_filter').val();
                    $('#selected_date').val(slider_dates[ui.value]);

                    if (date_filter == 'month') {
                        var month = parseInt(ui.value) + 1; 
                        if (month < 10) {
                            month = '0' + month.toString();
                        }

                        $('#month').val(month);
                    }

                    if (autoplay_timeout == undefined) {
                        var timeout = undefined;
                        if (timeout != undefined) {
                            window.clearTimeout(timeout);
                        }

                        timeout = window.setTimeout(arr_traffic.getTrafficData, 500);
                    }
                }
            });
            arr_traffic.setupSenegalMap();
        })
        .fail(function() {

        });
    },

    setupSenegalMap: function() {
        var width = $('#map_container').width();
        var height = $('#map_container').height();
        $.getJSON($GEOJSON_FILE, function(geojson_data) {
            map = new Datamap({
                element: document.getElementById('map_container'),
                responsive: true,
                scope: 'senegal_map',
                geographyConfig: {
                    dataUrl: $TOPOJSON_FILE
                },
                fills: {
                    defaultFill: '#C6DBEF'
                },
                setProjection: function(element, options) {
                    var projection = d3.geo.mercator().scale(1).translate([0,0]);
                    // create a path generator.
                    var path = d3.geo.path().projection(projection);
                    // compute bounds of a point of interest, then derive scale and translate
                    var b = path.bounds(geojson_data);
                    var s = .95 / Math.max((b[1][0] - b[0][0]) / width, (b[1][1] - b[0][1]) / height);
                    var t = [(width - s * (b[1][0] + b[0][0])) / 2, (height - s * (b[1][1] + b[0][1])) / 2];
                    // update the projection to use computed scale and translate....
                    projection.scale(s).translate(t);

                    return {path: path, projection: projection};
                },
                bubblesConfig: {
                    borderWidth: 0,
                },
                done: function(datamap) {
                    arr_traffic.getTrafficData();
                }
            });

        });
    },

    getTrafficData: function(change_date) {
        // when called from autoplay
        if (change_date) {
            var date = $('#choose_date').slider('value');
            $('#choose_date').slider('value', date + 1);

            if (date + 1 == slider_dates.length - 1) {
                $('#autoplay').attr('checked', false).trigger('change');
            }
        }

        var data = {
            traffic: $('#traffic').val(),
            direction: $('#direction').val(),
            month: $('#month').val(),
            month_raw: $('#month').find(':selected').data('raw'),
            date_filter: $('#date_filter').val(),
            date: slider_dates[$('#choose_date').slider('value')]
        };

        $.when($.ajax({
            url: $SCRIPT_ROOT + 'getTrafficData',
            data: data,
            type: 'post',
            dataType: 'json'
        }))
        .then(function(response) {
            arr_traffic.updateMap(response.result);
            $('#loading').addClass('hidden');
        })
        .fail(function() {

        });
    },

    updateMap: function(data) {
        var sites_num = data.length;
        var site_colors = {};

        var domain = [];
        for (var i in data) {
            domain.push(data[i][1]);
        }

        var range = [];
        var rainbow = new Rainbow();
        rainbow.setNumberRange(1, sites_num);
        rainbow.setSpectrum('#C6DBEF', '#084594');
        for (var i = 0; i < sites_num + 1; i++) {
            var hexColour = rainbow.colourAt(i);
            range.push('#' + hexColour);
        }

        var color = d3.scale.threshold()
            .domain(domain)
            .range(range);

        for (var i in data) {
            site_colors['sn_' + data[i][0]] = color(data[i][1]);
        }

        map.updateChoropleth(site_colors);
    },

    optionsChange: function(field) {
        // trigger change event to disable autoplay
        $('#autoplay').attr('checked', false).trigger('change');

        if (field == 'direction' || field == 'month') {
           arr_traffic.updateDateSlider();
        } else if (field == 'date_filter') {
            var date_filter = $('#date_filter').val();
            if (date_filter == 'month') {
                $('#choose_month_wrap').addClass('hidden');
            } else {
                $('#choose_month_wrap').removeClass('hidden');
            }

            arr_traffic.updateDateSlider();
        } else {
            $('#choose_date').slider('value', 0);
        }
    },

    updateDateSlider: function() {
        var date_filter = $('#date_filter').val();

        if (date_filter == 'month') {
            arr_traffic.updateDateSliderAfter(months_list);
        } else {
            if (date_filter == 'day') {
                var data = {
                    direction: $('#direction').val(),
                    month: $('#month').val(),
                    month_raw: $('#month').find(':selected').data('raw'),
                    date_filter: date_filter
                };
            } else {
                var data = {
                    direction: $('#direction').val(),
                    month: $('#month').val(),
                    month_raw: $('#month').find(':selected').data('raw'),
                    date_filter: date_filter
                };
            }

            $.when($.ajax({
                url: $SCRIPT_ROOT + 'getDateSliderValues',
                data: data,
                type: 'post',
                dataType: 'json'
            }))
            .then(function(response) {
                arr_traffic.updateDateSliderAfter(response.result);
            })
            .fail(function() {

            });
        }
    },

    updateDateSliderAfter: function(values) {
        slider_dates = values;
        var options = {
            disabled: false,
            value: 0,
            min: 0,
            max: slider_dates.length - 1,
        };

        $('#choose_date').slider('option', options);
    },
};

$(function() {
    arr_traffic.init();
});