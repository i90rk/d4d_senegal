import pandas as pd
import numpy as np
import time
import glob
import re
import datetime

'''
# process files from SET1 - number of outgoing/incoming calls and total outgoing/incoming call duration
set1_files = glob.glob('data_initial/DATA/SET1/SET1V_*.CSV')
for file_name in set1_files:
	print 'start import ' + file_name + ': ' + time.strftime("%d/%m/%Y %H:%M:%S")
	file_number = re.search('SET1V_(.+?).CSV', file_name).group(1)
	aa_traffic_columns = ['timestamp', 'outgoing_site_id', 'incoming_site_id', 'number_of_calls', 'total_call_duration']
	output_file_name_out = 'data_processed/arr_traffic/outgoing_traffic_per_arrondissment_' + file_number + '.csv'
	output_file_name_in = 'data_processed/arr_traffic/incoming_traffic_per_arrondissment_' + file_number + '.csv'
	# number of lines to process at each iteration
	chunksize = 100000
	# antenna-to-antenna traffic
	aa_traffic = pd.read_csv(file_name, names=aa_traffic_columns, header=None, chunksize=chunksize)
	# anntena arrondisment and coordinates
	coordinates = pd.read_csv('data_initial/SITE_ARR_LONLAT.CSV')

	for traffic in aa_traffic:
		traffic = traffic.merge(
			coordinates,
			left_on='outgoing_site_id',
			right_on='site_id',
			how='left')\
		.merge(
			coordinates,
			left_on='incoming_site_id',
			right_on='site_id',
			how='left')

		# sum number of outgoing calls per date, per arrondissment (for query statement is needed numexpr package)
		traffic_out = traffic.query('arr_id_x != arr_id_y')\
			.groupby(['timestamp', 'arr_id_x'], as_index=False)\
			.agg({'number_of_calls': np.sum, 'total_call_duration': np.sum})

		# write this chunk in the file with final results
		traffic_out.to_csv(
			output_file_name_out,
			header=False,
			index=False,
			mode='a+')

		# sum number of incoming calls per date, per arrondissment (for query statement is needed numexpr package)
		traffic_in = traffic.query('arr_id_x != arr_id_y')\
			.groupby(['timestamp', 'arr_id_y'], as_index=False)\
			.agg({'number_of_calls': np.sum, 'total_call_duration': np.sum})

		# write this chunk in the file with final results
		traffic_in.to_csv(
			output_file_name_in,
			header=False,
			index=False,
			mode='a+')

	# We need to make final aggregation because there will be multiple records with same
	# timestamp and arr_id in the file with the results (this is because we process the original file in chunks)
	
	# read the file with final results and set header columns
	final_data_out_columns = ['timestamp', 'arr_id', 'number_of_outgoing_calls', 'total_outgoing_call_duration']
	final_data_out = pd.read_csv(output_file_name_out, names=final_data_out_columns, header=None)
	# final aggregation
	final_data_out = final_data_out.groupby(['timestamp', 'arr_id'], as_index=False)\
		.agg({'number_of_outgoing_calls': np.sum, 'total_outgoing_call_duration': np.sum})

	final_data_out.to_csv(
			output_file_name_out,
			index=False,
			mode='w+')

	# read the file with final results and set header columns
	final_data_in_columns = ['timestamp', 'arr_id', 'number_of_incoming_calls', 'total_incoming_call_duration']
	final_data_in = pd.read_csv(output_file_name_in, names=final_data_in_columns, header=None)
	# final aggregation
	final_data_in = final_data_in.groupby(['timestamp', 'arr_id'], as_index=False)\
		.agg({'number_of_incoming_calls': np.sum, 'total_incoming_call_duration': np.sum})

	final_data_in.to_csv(
			output_file_name_in,
			index=False,
			mode='w+')

	print 'end import ' + file_name + ': ' + time.strftime("%d/%m/%Y %H:%M:%S")
'''



# process files from SET2 - number of residents and employees in each arrondissment
set2_files = glob.glob('data_initial/DATA/SET2/SET2_*.CSV')
# start and end of working time
wt_start = datetime.datetime.strptime('07:30:00', '%H:%M:%S').time()
wt_end = datetime.datetime.strptime('17:00:00', '%H:%M:%S').time()
output_file_residents = 'data_processed/population/population_residents.csv'
output_file_employees = 'data_processed/population/population_employees.csv'
chunksize = 100000
# site arrondissment and coordinates
coordinates = pd.read_csv('data_initial/SITE_ARR_LONLAT.CSV')
fg_traffic_columns = ['user_id', 'timestamp', 'site_id']

for file_name in set2_files:
	print 'start import ' + file_name + ': ' + time.strftime("%d/%m/%Y %H:%M:%S")
	# fine-grained traffic
	fg_traffic = pd.read_csv(file_name, names=fg_traffic_columns, header=None, chunksize=chunksize, parse_dates=['timestamp'])
	
	for traffic in fg_traffic:
		traffic = traffic.merge(coordinates, left_on='site_id', right_on='site_id', how='left')

		traffic['weekday'] = traffic['timestamp'].dt.weekday
		traffic['time'] = traffic['timestamp'].dt.time

		# count number of calls that each user has made in each arrondisment
		residents_condition = ((traffic.weekday < 5) & ((traffic.time >= wt_start) & (traffic.time <= wt_end)) | (traffic.weekday > 4))
		residents = pd.DataFrame({'calls' : traffic[residents_condition]\
				.groupby(['user_id', 'arr_id'], as_index=False)\
				.size()})\
			.reset_index()
		# find in which arrondissment each user has made maximum calls
		residents_max_calls = residents.groupby(['user_id'])['calls'].transform(max) == residents['calls']
		# count number of users for each arrondissment
		residents = pd.DataFrame({'residents' : residents[residents_max_calls].groupby(['arr_id'], as_index=False).size()}).reset_index()
		residents.to_csv(
			output_file_residents,
			header=False,
			index=False,
			mode='a+')

		# count number of calls that each user has made in each arrondisment
		employees_condition = ((traffic.weekday < 5) & ((traffic.time >= wt_start) & (traffic.time <= wt_end)))
		employees = pd.DataFrame({'calls' : traffic[employees_condition]\
				.groupby(['user_id', 'arr_id'], as_index=False)\
				.size()})\
			.reset_index()
		# find in which arrondissment each user has made maximum calls
		employees_max_calls = employees.groupby(['user_id'])['calls'].transform(max) == employees['calls']
		# count number of users for each arrondissment
		employees = pd.DataFrame({'employees' : employees[employees_max_calls].groupby(['arr_id'], as_index=False).size()}).reset_index()
		employees.to_csv(
			output_file_employees,
			header=False,
			index=False,
			mode='a+')

	# sum the number of residents for each arrondissment
	residents = pd.read_csv(output_file_residents, names=['arr_id', 'number_of_residents'], header=None)
	residents = residents.groupby(['arr_id'], as_index=False).agg({'number_of_residents': np.sum})
	residents.to_csv(
		output_file_residents,
		header=False,
		index=False,
		mode='w+')

	# sum the number of employees for each arrondissment
	employees = pd.read_csv(output_file_employees, names=['arr_id', 'number_of_employees'], header=None)
	employees = employees.groupby(['arr_id'], as_index=False).agg({'number_of_employees': np.sum})
	employees.to_csv(
		output_file_employees,
		header=False,
		index=False,
		mode='w+')

	print 'end import ' + file_name + ': ' + time.strftime("%d/%m/%Y %H:%M:%S")

# calculate percentage
residents = pd.read_csv(output_file_residents, names=['arr_id', 'number_of_residents'], header=None)
# calculate total number of users in all arrondissments
total_residents = residents['number_of_residents'].sum(axis=0)
# calculate percentage
residents['number_of_residents_in_percents'] = (residents.number_of_residents / total_residents) * 100
# round percents to 2 decimal places
residents['number_of_residents_in_percents'] = residents['number_of_residents_in_percents'].apply(lambda x: round(x, 2))
residents.to_csv(
	output_file_residents,
	index=False,
	mode='w+')

employees = pd.read_csv(output_file_employees, names=['arr_id', 'number_of_employees'], header=None)
total_employees = employees['number_of_employees'].sum(axis=0)
employees['number_of_employees_in_percents'] = (employees.number_of_employees / total_employees) * 100
employees['number_of_employees_in_percents'] = employees['number_of_employees_in_percents'].apply(lambda x: round(x, 2))
employees.to_csv(
	output_file_employees,
	index=False,
	mode='w+')

# http://stackoverflow.com/questions/6996603/how-do-i-delete-a-file-or-folder-in-python