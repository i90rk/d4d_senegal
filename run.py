# Run a server.
from app import app

app.config.from_object('config')
app.run(host=app.config['HOST'], port=app.config['PORT'])