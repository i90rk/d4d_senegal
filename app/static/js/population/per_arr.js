var per_arr = {
	map: null,

	init: function() {
		$('#loading').removeClass('hidden');
		selectMenuItem('menu_per_arr');
		this.getPopulationData();
		per_arr.events();
	},

	events: function() {
		$(window).on('resize', function() {
            map.resize();
        });
	},

	getPopulationData: function() {
		$.when($.ajax({
			url: $SCRIPT_ROOT + 'getPopulationData',
			type: 'get',
			dataType: 'json'
		}))
		.then(function(result) {
			per_arr.setupSenegalMap(per_arr.buildPopulationData(result));
		})
		.fail(function(error) {
			console.log(error);
		})
	},

	buildPopulationData: function(result) {
		var pop_greater = []; // population is greater than employees
		var pop_less = []; // population is less than employees
		var pop_equal = []; // population is equal to employees

		for (var i = 0; i < result.residents.length; i++) {
			var data = {id: 'sn_' + result.residents[i][0]};
			data.value = Math.abs(result.residents[i][1] - result.employees[i][1]);
			data.residents = result.residents[i][2];
			data.residents_no = result.residents[i][1];
			data.employees = result.employees[i][2];
			data.employees_no = result.employees[i][1];

			if (result.residents[i][2] > result.employees[i][2]) {
				pop_greater.push(data);
			} else if (result.residents[i][2] < result.employees[i][2]) {
				pop_less.push(data);
			} else {
				pop_equal.push(data);
			}
		}

		var mapData = per_arr.generateMapData([
			pop_greater.sort(per_arr.sortPopulationData('value')),
			pop_less.sort(per_arr.sortPopulationData('value')),
			pop_equal.sort(per_arr.sortPopulationData('value'))
		]);

		return mapData;
	},

	generateMapData: function(data) {
		var population_data = {};
		var map_colors = {};
		var color_gradients = [
			{start: '#ff0084', end: '#33001b'}, // red gradient
			{start: '#93F9B9', end: '#1D976C'}, // green gradient
			{start: '#26D0CE', end: '#1A2980'}, // blue gradinet
		];

		for (var i in data) {
			var color_func = per_arr.generateGradientColors(data[i], color_gradients[i]);

			for (var j in data[i]) {
				population_data[data[i][j].id] = {
					arr_id: data[i][j].id,
					residents: data[i][j].residents,
					residents_no: data[i][j].residents_no,
					employees: data[i][j].employees,
					employees_no: data[i][j].employees_no,
				};

				map_colors[data[i][j].id] = color_func(data[i][j].value);
			}
		}

		return [population_data, map_colors];
	},

	generateGradientColors: function(data, color_gradients) {
		var sites_num = data.length;

		var domain = [];
        for (var i in data) {
            domain.push(data[i]['value']);
        }

        var range = [];
        var rainbow = new Rainbow();
        rainbow.setNumberRange(1, sites_num);
        rainbow.setSpectrum(color_gradients.start, color_gradients.end);
        for (var i = 0; i < sites_num + 1; i++) {
            var hexColour = rainbow.colourAt(i);
            range.push('#' + hexColour);
        }

        var color = d3.scale.threshold()
            .domain(domain)
            .range(range);

        return color;
	},

	sortPopulationData: function(key) {
		return function(a, b) {
			if (a[key] < b[key]) {
				return -1;
			}

			if (a[key] > b[key]) {
				return 1;
			}

			return 0;
		};
	},

	setupSenegalMap: function(map_data) {
		var width = $('#map_container').width();
        var height = $('#map_container').height();
        $.getJSON($GEOJSON_FILE, function(geojson_data) {
            map = new Datamap({
                element: document.getElementById('map_container'),
                responsive: true,
                scope: 'senegal_map',
                data: map_data[0],
                geographyConfig: {
                    dataUrl: $TOPOJSON_FILE,
					popupTemplate: function(geo, data) {
						return [
							'<div class="hoverinfo">',
							'<strong>' + geo.properties.name + '</strong><br/>',
							'Residents: ' + data.residents + '% (' + data.residents_no + ')<br/>',
							'Employees: ' + data.employees + '% (' + data.employees_no + ')<br/>',
							'</div>'
						].join('');
					}
                },
				fills: {
					defaultFill: '#C6DBEF'
				},
                setProjection: function(element, options) {
                    var projection = d3.geo.mercator().scale(1).translate([0,0]);
                    // create a path generator.
                    var path = d3.geo.path().projection(projection);
                    // compute bounds of a point of interest, then derive scale and translate
                    var b = path.bounds(geojson_data);
                    var s = .95 / Math.max((b[1][0] - b[0][0]) / width, (b[1][1] - b[0][1]) / height);
                    var t = [(width - s * (b[1][0] + b[0][0])) / 2, (height - s * (b[1][1] + b[0][1])) / 2];
                    // update the projection to use computed scale and translate....
                    projection.scale(s).translate(t);

                    return {path: path, projection: projection};
                },
                bubblesConfig: {
                    borderWidth: 0,
                },
                done: function(datamap) {
					datamap.updateChoropleth(map_data[1]);
					$('#loading').addClass('hidden');
                }
            });
        });
	},
};

$(function() {
	per_arr.init();
});