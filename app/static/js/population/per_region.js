var per_region = {
	map: null,

	init: function() {
		$('#loading').removeClass('hidden');
		selectMenuItem('menu_per_region');
		per_region.getRegionPopulationData();
		per_region.events();
	},

	events: function() {
		$(window).on('resize', function() {
            map.resize();
        });
	},

	getRegionPopulationData: function() {
		$.when($.ajax({
			url: $SCRIPT_ROOT + 'getRegionPopulationData',
			type: 'get',
			dataType: 'json'
		}))
		.then(function(result) {
			per_region.setupSenegalMap(per_region.buildRegionPopulationData(result));
		})
		.fail(function(error) {
			console.log(error);
		})
	},

	buildRegionPopulationData: function(reg_data) {
		var pop_greater = []; // population is greater than employees
		var pop_less = []; // population is less than employees
		var pop_equal = []; // population is equal to employees

		for (var i = 0; i < reg_data.residents.length; i++) {
			var data = {id: 'sn_reg_' + reg_data.residents[i][0]};
			data.value = Math.abs(reg_data.residents[i][1] - reg_data.employees[i][1]);
			data.residents = reg_data.residents[i][2];
			data.residents_no = reg_data.residents[i][1];
			data.employees = reg_data.employees[i][2];
			data.employees_no = reg_data.employees[i][1];

			if (reg_data.residents[i][2] > reg_data.employees[i][2]) {
				pop_greater.push(data);
			} else if (reg_data.residents[i][2] < reg_data.employees[i][2]) {
				pop_less.push(data);
			} else {
				pop_equal.push(data);
			}
		}

		var mapData = per_region.generateMapData([
			pop_greater.sort(per_region.sortPopulationData('value')),
			pop_less.sort(per_region.sortPopulationData('value')),
			pop_equal.sort(per_region.sortPopulationData('value'))
		]);

		return mapData;
	},

	generateMapData: function(data) {
		var population_data = {};
		var map_colors = {};
		var color_gradients = [
			{start: '#ff0084', end: '#33001b'}, // red gradient
			{start: '#93F9B9', end: '#1D976C'}, // green gradient
			{start: '#26D0CE', end: '#1A2980'}, // blue gradinet
		];

		for (var i in data) {
			var color_func = per_region.generateGradientColors(data[i], color_gradients[i]);

			for (var j in data[i]) {
				population_data[data[i][j].id] = {
					region_id: data[i][j].id,
					residents: data[i][j].residents,
					residents_no: data[i][j].residents_no,
					employees: data[i][j].employees,
					employees_no: data[i][j].employees_no,
				};

				map_colors[data[i][j].id] = color_func(data[i][j].value);
			}
		}

		return [population_data, map_colors];
	},

	sortPopulationData: function(key) {
		return function(a, b) {
			if (a[key] < b[key]) {
				return -1;
			}

			if (a[key] > b[key]) {
				return 1;
			}

			return 0;
		};
	},

	generateGradientColors: function(data, color_gradients) {
		var sites_num = data.length;

		var domain = [];
        for (var i in data) {
            domain.push(data[i]['value']);
        }

        var range = [];
        var rainbow = new Rainbow();
        rainbow.setNumberRange(1, sites_num);
        rainbow.setSpectrum(color_gradients.start, color_gradients.end);
        for (var i = 0; i < sites_num + 1; i++) {
            var hexColour = rainbow.colourAt(i);
            range.push('#' + hexColour);
        }

        var color = d3.scale.threshold()
            .domain(domain)
            .range(range);

        return color;
	},

	setupSenegalMap: function(map_data) {
        var width = $('#map_container').width();
        var height = $('#map_container').height();
        $.getJSON($GEOJSON_REGIONS_FILE, function(geojson_data) {
            map = new Datamap({
                element: document.getElementById('map_container'),
                responsive: true,
                scope: 'senegal_map',
                data: map_data[0],
                geographyConfig: {
                    dataUrl: $TOPOJSON_REGIONS_FILE,
                    popupTemplate: function(geo, data) {
						return [
							'<div class="hoverinfo">',
							'<strong>' + geo.properties.name + '</strong><br/>',
							'Residents: ' + data.residents + '% (' + data.residents_no + ')<br/>',
							'Employees: ' + data.employees + '% (' + data.employees_no + ')<br/>',
							'</div>'
						].join('');
					}
                },
                fills: {
                    defaultFill: '#C6DBEF'
                },
                setProjection: function(element, options) {
                    var projection = d3.geo.mercator().scale(1).translate([0,0]);
                    // create a path generator.
                    var path = d3.geo.path().projection(projection);
                    // compute bounds of a point of interest, then derive scale and translate
                    var b = path.bounds(geojson_data);
                    var s = .95 / Math.max((b[1][0] - b[0][0]) / width, (b[1][1] - b[0][1]) / height);
                    var t = [(width - s * (b[1][0] + b[0][0])) / 2, (height - s * (b[1][1] + b[0][1])) / 2];
                    // update the projection to use computed scale and translate....
                    projection.scale(s).translate(t);

                    return {path: path, projection: projection};
                },
                bubblesConfig: {
                    borderWidth: 0,
                },
                done: function(datamap) {
                	datamap.updateChoropleth(map_data[1]);
                    $('#loading').addClass('hidden');
                }
            });
        });
    },
};

$(function() {
    per_region.init();
});