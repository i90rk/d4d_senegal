# Import flask and template operators
from flask import Flask, render_template, redirect, url_for, request

# Define the WSGI application object
app = Flask(__name__)

# Configurations
app.config.from_object('config')

# Import a module/component using its blueprint handler variable
from app.arrondissment_traffic.controllers import arrondissment_traffic
from app.population.controllers import population
# Register blueprint(s)
app.register_blueprint(arrondissment_traffic, url_prefix='/arr_traffic')
app.register_blueprint(population, url_prefix='/population')

# Sample HTTP error handling
@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404

@app.route('/')
def index():
	return redirect(url_for('arrondissment_traffic.index'))