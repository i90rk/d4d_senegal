var per_square_km = {
	map: null,

	init: function() {
		$('#loading').removeClass('hidden');
		selectMenuItem('menu_per_square_km');
		per_square_km.calcPopulationPerSquareKm();
		per_square_km.events();
	},

	events: function() {
		$(window).on('resize', function() {
            map.resize();
        });
	},

	calcPopulationPerSquareKm: function() {
		$.when($.ajax({
			url: $SCRIPT_ROOT + 'calcPopulationPerSquareKm',
			type: 'get',
			dataType: 'json'
		}))
		.then(function(result) {
			per_square_km.setupSenegalMap(per_square_km.generateMapData(result.result));
		})
		.fail(function(error) {
			console.log(error);
		})
	},

	generateMapData: function(data) {
        var regions_num = data.length;
        var population_data = {};
		var map_colors = {};

        var domain = [];
        for (var i in data) {
            domain.push(data[i][5]);
        }

        var range = [];
        var rainbow = new Rainbow();
        rainbow.setNumberRange(1, regions_num);
        rainbow.setSpectrum('#C6DBEF', '#084594');
        for (var i = 0; i < regions_num + 1; i++) {
            var hexColour = rainbow.colourAt(i);
            range.push('#' + hexColour);
        }

        var color = d3.scale.threshold()
            .domain(domain)
            .range(range);

        for (var i in data) {
            population_data['sn_reg_' + data[i][0]] = {
				region_id: data[i][0],
				density: data[i][5],
			};

			map_colors['sn_reg_' + data[i][0]] = color(data[i][5]);
        }

        return [population_data, map_colors];
    },

	setupSenegalMap: function(map_data) {
        var width = $('#map_container').width();
        var height = $('#map_container').height();
        $.getJSON($GEOJSON_REGIONS_FILE, function(geojson_data) {
            map = new Datamap({
                element: document.getElementById('map_container'),
                responsive: true,
                scope: 'senegal_map',
                data: map_data[0],
                geographyConfig: {
                    dataUrl: $TOPOJSON_REGIONS_FILE,
                    popupTemplate: function(geo, data) {
						return [
							'<div class="hoverinfo">',
							'<strong>' + geo.properties.name + '</strong><br/>',
							'Density: ' + data.density + ' (people/km<sup>2</sup>)<br/>',
							'</div>'
						].join('');
					}
                },
                fills: {
                    defaultFill: '#C6DBEF'
                },
                setProjection: function(element, options) {
                    var projection = d3.geo.mercator().scale(1).translate([0,0]);
                    // create a path generator.
                    var path = d3.geo.path().projection(projection);
                    // compute bounds of a point of interest, then derive scale and translate
                    var b = path.bounds(geojson_data);
                    var s = .95 / Math.max((b[1][0] - b[0][0]) / width, (b[1][1] - b[0][1]) / height);
                    var t = [(width - s * (b[1][0] + b[0][0])) / 2, (height - s * (b[1][1] + b[0][1])) / 2];
                    // update the projection to use computed scale and translate....
                    projection.scale(s).translate(t);

                    return {path: path, projection: projection};
                },
                bubblesConfig: {
                    borderWidth: 0,
                },
                done: function(datamap) {
                	datamap.updateChoropleth(map_data[1]);
                    $('#loading').addClass('hidden');
                }
            });
        });
    }
};

$(function() {
	per_square_km.init();
});