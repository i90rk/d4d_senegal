import pandas as pd
import numpy as np
# Import flask dependencies
from flask import Blueprint, request, render_template, g, redirect, url_for, jsonify
from collections import OrderedDict
import datetime

# Define the blueprint arrondissment_traffic
arrondissment_traffic = Blueprint('arrondissment_traffic', __name__)

@arrondissment_traffic.route('/', methods=['GET', 'POST'])
def index():
    return render_template('arrondissment_traffic/arr_traffic.html')

@arrondissment_traffic.route('/getDateSliderValues', methods=['POST'])
def getDateSliderValues():
	direction = request.form['direction']
	month = request.form['month']
	month_raw = request.form['month_raw']
	date_filter = request.form['date_filter']

	if direction == 'out':
		file_name = 'outgoing_traffic_per_arrondissment_' + month + '.csv'
	else:
		file_name = 'incoming_traffic_per_arrondissment_' + month + '.csv'

	csv_file = 'app/static/data_processed/arr_traffic/' + file_name
	df = pd.read_csv(csv_file)

	if date_filter == 'day':
		df['date_index'] = pd.to_datetime(df['timestamp'])
		df['month'] = df['date_index'].dt.month
		filtered_res = df['timestamp'][df.month == int(month_raw)].values.tolist()
		result = []

		for date in filtered_res:
			split_values = date.split(' ')
			result.append(split_values[0])

		result = OrderedDict.fromkeys(result).keys()

	else:
		df = pd.read_csv(csv_file)
		result = pd.unique(df.timestamp.ravel()).tolist()

	return jsonify(result=result)

@arrondissment_traffic.route('/getTrafficData', methods=['POST'])
def getTrafficData():
	direction = request.form['direction']
	traffic = request.form['traffic']
	month = request.form['month']
	month_raw = request.form['month_raw']
	date_filter = request.form['date_filter']
	date = request.form['date']

	file_name = ''
	column = ''

	if direction == 'out':
		file_name = 'outgoing_traffic_per_arrondissment_' + month + '.csv'
		if traffic == 'call_dur':
			column = 'total_outgoing_call_duration'
		else:
			column = 'number_of_outgoing_calls'
	else:
		file_name = 'incoming_traffic_per_arrondissment_' + month + '.csv'
		if traffic == 'call_dur':
			column = 'total_incoming_call_duration'
		else:
			column = 'number_of_incoming_calls'

	csv_file = 'app/static/data_processed/arr_traffic/' + file_name
	df = pd.read_csv(csv_file)

	if date_filter == 'hour':
		result = df[['arr_id', column]][df.timestamp == date].sort(column, ascending=1)
	elif date_filter == 'day':
		# pass
		df['date_index'] = pd.to_datetime(df['timestamp'])
		df['day'] = df['date_index'].dt.day
		day = datetime.datetime.strptime(date, '%Y-%m-%d').date().day
		result = df[df.day == day]\
			.groupby(['arr_id'], as_index=False)\
			.agg({column: np.sum})\
			.sort(column, ascending=1)
	else:
		result = df.groupby(['arr_id'], as_index=False)\
			.agg({column: np.sum})\
			.sort(column, ascending=1)

	result = normalizeData(result)

	return jsonify(result=result)

def normalizeData(data):
	data = data.values.tolist()
	list_length = len(data)
	min_val = data[0][1]
	max_val = data[list_length - 1][1]

	for i in range(len(data)):
		data[i][1] = float(data[i][1] - min_val) / float(max_val - min_val)

	return data