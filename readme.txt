--- UPATSTVO ZA INSTALACIJA NA APLIKACIJATA (Linux i python 2.7) ---

1. Prevzemanje na proektot od git
2. Vnatre vo proektot se kreira virtuelna okolina
	- vo slucaj da ne e instalirana python okolina
		- sudo apt-get install build-essential python-dev
		- sudo apt-get install python-pip
		- sudo pip install virtualenv

	* cd patekata_do_proektot/
	* virtualenv venv (se kreira virtuelna okolina)
3. Aktiviranje na virtuelnata okolina
	* source venv/bin/activate
4. Instalacija na bibliotekite
	* pip install -r requirements.txt (ova mora da se izvrsi vo aktivirana virtuelna okolina)
5. Start na aplikacijata
	* python run.py
6. Linkot se otvora vo browser (linkot od serverot daden vo terminalot)



--- ZA PROCESIRANJE NA PODATOCITE (NAPOMENA: PODATOCITE SE VEKJE IZGENERIRANI, POTREBNO E SAMO DA SE INSTALIRA APLIKACIJATA) ---

1. pandas_generate_data.py treba da gi izgenerira podatocite so koi ke se raboti vo aplikacijata
2. Potrebno e da ima dva folderi na root vo proektot (ovie folderi ne se staveni pod git, se koristat samo da se dobijat podatoci)
	* data_initial - tuka treba da bidat podatocite od D4D Senegal, odnosno DATA folderot,
		zaedno so csv fajlovite so podatocite za oblastite (SENEGAL_ARR_V2.csv) i koordinatite (SITE_ARR_LONLAT.CSV)
	* data_processed - tuka ke se zapisuvaat izgeneriranite podatoci vo posebni folderi 
		(ovie folderi treba da bidat kreirani racno vo momentov, ke bide napraveno da se kreiraat preku kod)
	* kako treba da izgleda folderskata struktura pred da se procesiraat podatocite

	    |---project_name
			|--- app/
			|--- data_initial/
			|    |--- DATA/
			|    |    |--- SET1/
			|    |    |--- SET2/
			|    |    |--- SET3/
			|    |--- SENEGAL_ARR_V2.csv
			|    |--- SITE_ARR_LONLAT.CSV
			|--- data_processed/
			|    |--- arr_traffic/
			|    |--- population/
			|--- venv/

3. Otkako ke zavrsi procesiranjeto na podatocite, folderot data_processed se kopira vo app/static direktoriumot (od tuka aplikacijata gi cita podatocite, ovoj folder e staven pod git)
4. Izvrsuvanje na pandas_generate_data.py
	* vo slucaj da ne e postavena okolinata za python, potrebno e da se izvrsat cekorite od upatstvoto za instalacija (2, 3, 4)
	* python pandas_generate_data (normalno izvrsuvanje)
	* nohup python -u pandas_generate_data.py & (izvrsuvanje kako background process)
5. Predvideno izvrsuvanje na skriptata za SET1 i SET2 e 3 casa (vremeto e varijabilno vo zavisnost od hardverot)
